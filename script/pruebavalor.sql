-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-03-2018 a las 03:24:16
-- Versión del servidor: 10.1.29-MariaDB
-- Versión de PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pruebavalor`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `RUC` bigint(16) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `telefono` bigint(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`RUC`, `nombre`, `direccion`, `telefono`) VALUES
(204265478, 'Saray Varela', 'Santa Teresa', 41457894132),
(1245678900, 'Gabriela Anselmi', 'Unidad Vecinal', 4167894521),
(2100045612, 'Rafael Soto', 'San Cristóbal', 4167894521);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion`
--

CREATE TABLE `cotizacion` (
  `id` int(11) NOT NULL,
  `iva` int(4) NOT NULL,
  `CLIENTE_RUC` bigint(16) NOT NULL,
  `VENDEDOR_RUC` bigint(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cotizacion`
--

INSERT INTO `cotizacion` (`id`, `iva`, `CLIENTE_RUC`, `VENDEDOR_RUC`) VALUES
(33, 12, 204265478, 456789),
(34, 16, 1245678900, 456789);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion_final`
--

CREATE TABLE `cotizacion_final` (
  `ID` int(11) NOT NULL,
  `total` double NOT NULL,
  `iva` double NOT NULL,
  `fecha` date NOT NULL,
  `PAQUETE_id` int(11) NOT NULL,
  `COTIZACION_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cotizacion_final`
--

INSERT INTO `cotizacion_final` (`ID`, `total`, `iva`, `fecha`, `PAQUETE_id`, `COTIZACION_id`) VALUES
(30, 0, 0, '0000-00-00', 2, 33),
(31, 0, 0, '0000-00-00', 1, 34),
(32, 0, 0, '0000-00-00', 2, 34);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion_producto`
--

CREATE TABLE `cotizacion_producto` (
  `id` int(11) NOT NULL,
  `PRODUCTO_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `COTIZACION_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cotizacion_producto`
--

INSERT INTO `cotizacion_producto` (`id`, `PRODUCTO_id`, `cantidad`, `COTIZACION_id`) VALUES
(42, 1, 1, 33),
(43, 1, 1, 34),
(44, 4, 1, 34);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquete`
--

CREATE TABLE `paquete` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paquete`
--

INSERT INTO `paquete` (`id`, `nombre`) VALUES
(1, 'Salto Angel'),
(2, 'Colonia Tovar'),
(3, 'Gran Sabana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquete_producto`
--

CREATE TABLE `paquete_producto` (
  `id` int(11) NOT NULL,
  `PRODUCTO_id` int(11) NOT NULL,
  `PAQUETE_id` int(11) NOT NULL,
  `descuento` double NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `subtotal` double NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paquete_producto`
--

INSERT INTO `paquete_producto` (`id`, `PRODUCTO_id`, `PAQUETE_id`, `descuento`, `descripcion`, `subtotal`, `cantidad`) VALUES
(1, 1, 1, 10, 'Maíz natural y queso', 500000, 2),
(2, 3, 2, 15, 'Maíz natural y queso', 500000, 2),
(3, 3, 3, 10, 'Queso paisa', 1000000, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `precio` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `descripcion`, `precio`) VALUES
(1, 'Cachapa de queso mozzarella', 250000),
(2, 'Cachapa de queso guayanés', 250000),
(3, 'Cachapa de queso tipo paisa', 250000),
(4, 'Papelón con limón', 50000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendedor`
--

CREATE TABLE `vendedor` (
  `RUC` bigint(16) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vendedor`
--

INSERT INTO `vendedor` (`RUC`, `nombre`) VALUES
(456789, 'Mariana Cardenas'),
(1452358454, 'Yoner Varela');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`RUC`);

--
-- Indices de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD PRIMARY KEY (`id`,`CLIENTE_RUC`,`VENDEDOR_RUC`),
  ADD KEY `fk_COTIZACION_CLIENTE1_idx` (`CLIENTE_RUC`),
  ADD KEY `fk_COTIZACION_VENDEDOR1_idx` (`VENDEDOR_RUC`);

--
-- Indices de la tabla `cotizacion_final`
--
ALTER TABLE `cotizacion_final`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_COTIZACION_FINAL_PAQUETE1_idx` (`PAQUETE_id`),
  ADD KEY `fk_COTIZACION_FINAL_COTIZACION1_idx` (`COTIZACION_id`);

--
-- Indices de la tabla `cotizacion_producto`
--
ALTER TABLE `cotizacion_producto`
  ADD PRIMARY KEY (`id`,`PRODUCTO_id`,`COTIZACION_id`),
  ADD KEY `fk_COTIZACION_PRODUCTO_PRODUCTO1_idx` (`PRODUCTO_id`),
  ADD KEY `fk_COTIZACION_PRODUCTO_COTIZACION1_idx` (`COTIZACION_id`);

--
-- Indices de la tabla `paquete`
--
ALTER TABLE `paquete`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paquete_producto`
--
ALTER TABLE `paquete_producto`
  ADD PRIMARY KEY (`id`,`PRODUCTO_id`,`PAQUETE_id`),
  ADD KEY `fk_PAQUETE_PRODUCTO_PRODUCTO1_idx` (`PRODUCTO_id`),
  ADD KEY `fk_PAQUETE_PRODUCTO_PAQUETE1_idx` (`PAQUETE_id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vendedor`
--
ALTER TABLE `vendedor`
  ADD PRIMARY KEY (`RUC`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `cotizacion_final`
--
ALTER TABLE `cotizacion_final`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `cotizacion_producto`
--
ALTER TABLE `cotizacion_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `paquete`
--
ALTER TABLE `paquete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `paquete_producto`
--
ALTER TABLE `paquete_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD CONSTRAINT `fk_COTIZACION_CLIENTE1` FOREIGN KEY (`CLIENTE_RUC`) REFERENCES `cliente` (`RUC`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_COTIZACION_VENDEDOR1` FOREIGN KEY (`VENDEDOR_RUC`) REFERENCES `vendedor` (`RUC`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cotizacion_final`
--
ALTER TABLE `cotizacion_final`
  ADD CONSTRAINT `fk_COTIZACION_FINAL_COTIZACION1` FOREIGN KEY (`COTIZACION_id`) REFERENCES `cotizacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_COTIZACION_FINAL_PAQUETE1` FOREIGN KEY (`PAQUETE_id`) REFERENCES `paquete` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cotizacion_producto`
--
ALTER TABLE `cotizacion_producto`
  ADD CONSTRAINT `fk_COTIZACION_PRODUCTO_COTIZACION1` FOREIGN KEY (`COTIZACION_id`) REFERENCES `cotizacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_COTIZACION_PRODUCTO_PRODUCTO1` FOREIGN KEY (`PRODUCTO_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `paquete_producto`
--
ALTER TABLE `paquete_producto`
  ADD CONSTRAINT `fk_PAQUETE_PRODUCTO_PAQUETE1` FOREIGN KEY (`PAQUETE_id`) REFERENCES `paquete` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_PAQUETE_PRODUCTO_PRODUCTO1` FOREIGN KEY (`PRODUCTO_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
