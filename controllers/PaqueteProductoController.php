<?php

namespace app\controllers;

use Yii;
use app\models\PaqueteProducto;
use app\models\PaqueteProductoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaqueteProductoController implements the CRUD actions for PaqueteProducto model.
 */
class PaqueteProductoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaqueteProducto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaqueteProductoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaqueteProducto model.
     * @param integer $id
     * @param integer $PRODUCTO_id
     * @param integer $PAQUETE_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $PRODUCTO_id, $PAQUETE_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $PRODUCTO_id, $PAQUETE_id),
        ]);
    }

    /**
     * Creates a new PaqueteProducto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PaqueteProducto();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'PRODUCTO_id' => $model->PRODUCTO_id, 'PAQUETE_id' => $model->PAQUETE_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PaqueteProducto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $PRODUCTO_id
     * @param integer $PAQUETE_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $PRODUCTO_id, $PAQUETE_id)
    {
        $model = $this->findModel($id, $PRODUCTO_id, $PAQUETE_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'PRODUCTO_id' => $model->PRODUCTO_id, 'PAQUETE_id' => $model->PAQUETE_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PaqueteProducto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $PRODUCTO_id
     * @param integer $PAQUETE_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $PRODUCTO_id, $PAQUETE_id)
    {
        $this->findModel($id, $PRODUCTO_id, $PAQUETE_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaqueteProducto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $PRODUCTO_id
     * @param integer $PAQUETE_id
     * @return PaqueteProducto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $PRODUCTO_id, $PAQUETE_id)
    {
        if (($model = PaqueteProducto::findOne(['id' => $id, 'PRODUCTO_id' => $PRODUCTO_id, 'PAQUETE_id' => $PAQUETE_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
