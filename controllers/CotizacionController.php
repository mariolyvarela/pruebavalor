<?php

namespace app\controllers;

use Yii;
use app\models\Cotizacion;
use app\models\CotizacionProducto;
use app\models\CotizacionFinal;
use app\models\Model;
use app\models\CotizacionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CotizacionController implements the CRUD actions for Cotizacion model.
 */
class CotizacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cotizacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CotizacionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cotizacion model.
     * @param integer $id
     * @param string $CLIENTE_RUC
     * @param string $VENDEDOR_RUC
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $CLIENTE_RUC, $VENDEDOR_RUC)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $CLIENTE_RUC, $VENDEDOR_RUC),
        ]);
    }

    /**
     * Creates a new Cotizacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cotizacion();
        
        $model = new Cotizacion();
        $CotizacionProductos = [new CotizacionProducto];
        $CotizacionPaquetes = [new CotizacionFinal];

          if ($model->load(Yii::$app->request->post())) {
            $CotizacionProductos = Model::createMultiple(CotizacionProducto::classname());
            Model::loadMultiple($CotizacionProductos, Yii::$app->request->post());

            $CotizacionPaquetes = Model::createMultiple(CotizacionFinal::classname());
            Model::loadMultiple($CotizacionPaquetes, Yii::$app->request->post());

            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $model->save(false)) {    

                    foreach ($CotizacionProductos as $objCotizacionProducto) {
                        $objCotizacionProducto->COTIZACION_id = $model->id;
                        if (! ($flag = $objCotizacionProducto->save(false))) {
                            $transaction->rollBack();
                            break;
                        }
                    }

                    foreach ( $CotizacionPaquetes as $objCotizacionFinal) {
                        $objCotizacionFinal->COTIZACION_id = $model->id;
                        if (! ($flag = $objCotizacionFinal->save(false))) {
                            $transaction->rollBack();
                            break;
                        }
                    }

                }
                    if ($flag) {
                        $model->save(false);
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id, 'CLIENTE_RUC' => $model->CLIENTE_RUC, 'VENDEDOR_RUC' => $model->VENDEDOR_RUC]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }          
            }else{
    
    
                return $this->render('create', [
                    'model' => $model,
                    'CotizacionProductos' => (empty($CotizacionProductos)) ? [new CotizacionProducto] : $CotizacionProductos,
                    'CotizacionPaquetes' => (empty($CotizacionPaquetes)) ? [new CotizacionPaquete] : $CotizacionPaquetes
                ]);
            }
/*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'CLIENTE_RUC' => $model->CLIENTE_RUC, 'VENDEDOR_RUC' => $model->VENDEDOR_RUC]);
        }

        return $this->render('create', [
            'model' => $model,
        ]); */
    }

    /**
     * Updates an existing Cotizacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $CLIENTE_RUC
     * @param string $VENDEDOR_RUC
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $CLIENTE_RUC, $VENDEDOR_RUC)
    {
        $model = $this->findModel($id, $CLIENTE_RUC, $VENDEDOR_RUC);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'CLIENTE_RUC' => $model->CLIENTE_RUC, 'VENDEDOR_RUC' => $model->VENDEDOR_RUC]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cotizacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $CLIENTE_RUC
     * @param string $VENDEDOR_RUC
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $CLIENTE_RUC, $VENDEDOR_RUC)
    {
        $this->findModel($id, $CLIENTE_RUC, $VENDEDOR_RUC)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cotizacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $CLIENTE_RUC
     * @param string $VENDEDOR_RUC
     * @return Cotizacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $CLIENTE_RUC, $VENDEDOR_RUC)
    {
        if (($model = Cotizacion::findOne(['id' => $id, 'CLIENTE_RUC' => $CLIENTE_RUC, 'VENDEDOR_RUC' => $VENDEDOR_RUC])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
