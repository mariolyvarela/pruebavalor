<?php

namespace app\controllers;

use Yii;
use app\models\CotizacionProducto;
use app\models\CotizacionProductoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CotizacionProductoController implements the CRUD actions for CotizacionProducto model.
 */
class CotizacionProductoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CotizacionProducto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CotizacionProductoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CotizacionProducto model.
     * @param integer $id
     * @param integer $PRODUCTO_id
     * @param integer $COTIZACION_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $PRODUCTO_id, $COTIZACION_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $PRODUCTO_id, $COTIZACION_id),
        ]);
    }

    /**
     * Creates a new CotizacionProducto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CotizacionProducto();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'PRODUCTO_id' => $model->PRODUCTO_id, 'COTIZACION_id' => $model->COTIZACION_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CotizacionProducto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $PRODUCTO_id
     * @param integer $COTIZACION_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $PRODUCTO_id, $COTIZACION_id)
    {
        $model = $this->findModel($id, $PRODUCTO_id, $COTIZACION_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'PRODUCTO_id' => $model->PRODUCTO_id, 'COTIZACION_id' => $model->COTIZACION_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CotizacionProducto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $PRODUCTO_id
     * @param integer $COTIZACION_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $PRODUCTO_id, $COTIZACION_id)
    {
        $this->findModel($id, $PRODUCTO_id, $COTIZACION_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CotizacionProducto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $PRODUCTO_id
     * @param integer $COTIZACION_id
     * @return CotizacionProducto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $PRODUCTO_id, $COTIZACION_id)
    {
        if (($model = CotizacionProducto::findOne(['id' => $id, 'PRODUCTO_id' => $PRODUCTO_id, 'COTIZACION_id' => $COTIZACION_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
