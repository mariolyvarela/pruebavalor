<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property string $RUC
 * @property string $nombre
 * @property string $direccion
 * @property string $telefono
 *
 * @property Cotizacion[] $cotizacions
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['RUC', 'nombre'], 'required'],
            [['RUC', 'telefono'], 'integer'],
            [['nombre', 'direccion'], 'string', 'max' => 45],
            [['RUC'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'RUC' => 'Ruc',
            'nombre' => 'Nombre',
            'direccion' => 'Dirección',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacions()
    {
        return $this->hasMany(Cotizacion::className(), ['CLIENTE_RUC' => 'RUC']);
    }
}
