<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CotizacionFinal;

/**
 * CotizacionFinalSearch represents the model behind the search form of `app\models\CotizacionFinal`.
 */
class CotizacionFinalSearch extends CotizacionFinal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'PAQUETE_id', 'COTIZACION_PRODUCTO_id', 'COTIZACION_PRODUCTO_PRODUCTO_id', 'COTIZACION_PRODUCTO_COTIZACION_id', 'COTIZACION_PRODUCTO_COTIZACION_CLIENTE_RUC', 'COTIZACION_PRODUCTO_COTIZACION_VENDEDOR_RUC'], 'integer'],
            [['total', 'iva'], 'number'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CotizacionFinal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'total' => $this->total,
            'iva' => $this->iva,
            'fecha' => $this->fecha,
            'PAQUETE_id' => $this->PAQUETE_id,
            'COTIZACION_PRODUCTO_id' => $this->COTIZACION_PRODUCTO_id,
            'COTIZACION_PRODUCTO_PRODUCTO_id' => $this->COTIZACION_PRODUCTO_PRODUCTO_id,
            'COTIZACION_PRODUCTO_COTIZACION_id' => $this->COTIZACION_PRODUCTO_COTIZACION_id,
            'COTIZACION_PRODUCTO_COTIZACION_CLIENTE_RUC' => $this->COTIZACION_PRODUCTO_COTIZACION_CLIENTE_RUC,
            'COTIZACION_PRODUCTO_COTIZACION_VENDEDOR_RUC' => $this->COTIZACION_PRODUCTO_COTIZACION_VENDEDOR_RUC,
        ]);

        return $dataProvider;
    }
}
