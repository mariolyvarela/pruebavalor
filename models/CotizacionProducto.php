<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cotizacion_producto".
 *
 * @property int $id
 * @property int $PRODUCTO_id
 * @property int $cantidad
 * @property int $COTIZACION_id
 *
 * @property Cotizacion $cOTIZACION
 * @property Producto $pRODUCTO
 */
class CotizacionProducto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cotizacion_producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PRODUCTO_id', 'cantidad', 'COTIZACION_id'], 'required'],
            [['PRODUCTO_id', 'cantidad', 'COTIZACION_id'], 'integer'],
            [['COTIZACION_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cotizacion::className(), 'targetAttribute' => ['COTIZACION_id' => 'id']],
            [['PRODUCTO_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['PRODUCTO_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'PRODUCTO_id' => 'Producto ID',
            'cantidad' => 'Cantidad',
            'COTIZACION_id' => 'Cotizacion ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCOTIZACION()
    {
        return $this->hasOne(Cotizacion::className(), ['id' => 'COTIZACION_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUCTO()
    {
        return $this->hasOne(Producto::className(), ['id' => 'PRODUCTO_id']);
    }
}
