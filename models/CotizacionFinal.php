<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cotizacion_final".
 *
 * @property int $ID
 * @property double $total
 * @property double $iva
 * @property string $fecha
 * @property int $PAQUETE_id
 * @property int $COTIZACION_id
 *
 * @property Cotizacion $cOTIZACION
 * @property Paquete $pAQUETE
 */
class CotizacionFinal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cotizacion_final';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total', 'iva', 'fecha', 'PAQUETE_id', 'COTIZACION_id'], 'required'],
            [['total', 'iva'], 'number'],
            [['fecha'], 'safe'],
            [['PAQUETE_id', 'COTIZACION_id'], 'integer'],
            [['COTIZACION_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cotizacion::className(), 'targetAttribute' => ['COTIZACION_id' => 'id']],
            [['PAQUETE_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paquete::className(), 'targetAttribute' => ['PAQUETE_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'total' => 'Total',
            'iva' => 'Iva',
            'fecha' => 'Fecha',
            'PAQUETE_id' => 'Paquete ID',
            'COTIZACION_id' => 'Cotizacion ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCOTIZACION()
    {
        return $this->hasOne(Cotizacion::className(), ['id' => 'COTIZACION_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPAQUETE()
    {
        return $this->hasOne(Paquete::className(), ['id' => 'PAQUETE_id']);
    }
}
