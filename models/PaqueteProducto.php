<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paquete_producto".
 *
 * @property int $id
 * @property int $PRODUCTO_id
 * @property int $PAQUETE_id
 * @property double $descuento
 * @property string $descripcion
 * @property double $subtotal
 * @property int $cantidad
 *
 * @property Paquete $pAQUETE
 * @property Producto $pRODUCTO
 */
class PaqueteProducto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquete_producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PRODUCTO_id', 'PAQUETE_id', 'descuento', 'descripcion', 'subtotal', 'cantidad'], 'required'],
            [['PRODUCTO_id', 'PAQUETE_id', 'cantidad'], 'integer'],
            [['descuento', 'subtotal'], 'number'],
            [['descripcion'], 'string', 'max' => 45],
            [['PAQUETE_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paquete::className(), 'targetAttribute' => ['PAQUETE_id' => 'id']],
            [['PRODUCTO_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['PRODUCTO_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'PRODUCTO_id' => 'Producto ID',
            'PAQUETE_id' => 'Paquete ID',
            'descuento' => 'Descuento',
            'descripcion' => 'Descripcion',
            'subtotal' => 'Subtotal',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPAQUETE()
    {
        return $this->hasOne(Paquete::className(), ['id' => 'PAQUETE_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUCTO()
    {
        return $this->hasOne(Producto::className(), ['id' => 'PRODUCTO_id']);
    }
}
