<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaqueteProducto;

/**
 * PaqueteProductoSearch represents the model behind the search form of `app\models\PaqueteProducto`.
 */
class PaqueteProductoSearch extends PaqueteProducto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'PRODUCTO_id', 'PAQUETE_id', 'cantidad'], 'integer'],
            [['descuento', 'subtotal'], 'number'],
            [['descripcion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaqueteProducto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'PRODUCTO_id' => $this->PRODUCTO_id,
            'PAQUETE_id' => $this->PAQUETE_id,
            'descuento' => $this->descuento,
            'subtotal' => $this->subtotal,
            'cantidad' => $this->cantidad,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
