<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paquete".
 *
 * @property int $id
 * @property string $nombre
 *
 * @property CotizacionFinal[] $cotizacionFinals
 * @property PaqueteProducto[] $paqueteProductos
 */
class Paquete extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquete';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionFinals()
    {
        return $this->hasMany(CotizacionFinal::className(), ['PAQUETE_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaqueteProductos()
    {
        return $this->hasMany(PaqueteProducto::className(), ['PAQUETE_id' => 'id']);
    }
}
