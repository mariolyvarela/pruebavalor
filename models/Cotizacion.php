<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cotizacion".
 *
 * @property int $id
 * @property int $iva
 * @property string $CLIENTE_RUC
 * @property string $VENDEDOR_RUC
 *
 * @property Cliente $cLIENTERUC
 * @property Vendedor $vENDEDORRUC
 * @property CotizacionFinal[] $cotizacionFinals
 * @property CotizacionProducto[] $cotizacionProductos
 */
class Cotizacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cotizacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iva', 'CLIENTE_RUC', 'VENDEDOR_RUC'], 'required'],
            [['iva', 'CLIENTE_RUC', 'VENDEDOR_RUC'], 'integer'],
            [['CLIENTE_RUC'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['CLIENTE_RUC' => 'RUC']],
            [['VENDEDOR_RUC'], 'exist', 'skipOnError' => true, 'targetClass' => Vendedor::className(), 'targetAttribute' => ['VENDEDOR_RUC' => 'RUC']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'iva' => 'Iva',
            'CLIENTE_RUC' => 'Cliente  Ruc',
            'VENDEDOR_RUC' => 'Vendedor  Ruc',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCLIENTERUC()
    {
        return $this->hasOne(Cliente::className(), ['RUC' => 'CLIENTE_RUC']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVENDEDORRUC()
    {
        return $this->hasOne(Vendedor::className(), ['RUC' => 'VENDEDOR_RUC']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionFinals()
    {
        return $this->hasMany(CotizacionFinal::className(), ['COTIZACION_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionProductos()
    {
        return $this->hasMany(CotizacionProducto::className(), ['COTIZACION_id' => 'id']);
    }
}
