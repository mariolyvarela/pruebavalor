<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property int $id
 * @property string $descripcion
 * @property double $precio
 *
 * @property CotizacionProducto[] $cotizacionProductos
 * @property PaqueteProducto[] $paqueteProductos
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'precio'], 'required'],
            [['precio'], 'number'],
            [['descripcion'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripción',
            'precio' => 'Precio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionProductos()
    {
        return $this->hasMany(CotizacionProducto::className(), ['PRODUCTO_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaqueteProductos()
    {
        return $this->hasMany(PaqueteProducto::className(), ['PRODUCTO_id' => 'id']);
    }
}
