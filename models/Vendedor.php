<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vendedor".
 *
 * @property string $RUC
 * @property string $nombre
 *
 * @property Cotizacion[] $cotizacions
 */
class Vendedor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vendedor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['RUC', 'nombre'], 'required'],
            [['RUC'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['RUC'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'RUC' => 'Ruc',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacions()
    {
        return $this->hasMany(Cotizacion::className(), ['VENDEDOR_RUC' => 'RUC']);
    }
}
