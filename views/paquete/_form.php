<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Producto;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Paquete */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paquete-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
      
        <div class="row">
        
        <div class="panel panel-default">
        <div class="panel-heading"><h4> Productos</h4></div>
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 8, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $paquete_productos[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'PRODUCTO_id',
                    'descuento',
                    'descripcion',
                    'cantidad',
                    'subtotal',
                 ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($paquete_productos as $i => $paquete_producto): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Producto</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $paquete_producto->isNewRecord) {
                                echo Html::activeHiddenInput($paquete_producto, "[{$i}]id");
                            }
                        ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($paquete_producto, "[{$i}]PRODUCTO_id")->dropDownList(ArrayHelper::map(Producto::find()->all(),'id','descripcion'),
                            ['prompt'=>'Seleccionar Producto']) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($paquete_producto, "[{$i}]descripcion")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-12">
                                <?= $form->field($paquete_producto, "[{$i}]descuento")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($paquete_producto, "[{$i}]cantidad")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($paquete_producto, "[{$i}]subtotal")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>
        </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
