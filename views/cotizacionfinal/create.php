<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CotizacionFinal */

$this->title = 'Create Cotizacion Final';
$this->params['breadcrumbs'][] = ['label' => 'Cotizacion Finals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cotizacion-final-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
