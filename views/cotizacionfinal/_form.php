<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CotizacionFinal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cotizacion-final-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <?= $form->field($model, 'iva')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'PAQUETE_id')->textInput() ?>

    <?= $form->field($model, 'COTIZACION_PRODUCTO_id')->textInput() ?>

    <?= $form->field($model, 'COTIZACION_PRODUCTO_PRODUCTO_id')->textInput() ?>

    <?= $form->field($model, 'COTIZACION_PRODUCTO_COTIZACION_id')->textInput() ?>

    <?= $form->field($model, 'COTIZACION_PRODUCTO_COTIZACION_CLIENTE_RUC')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COTIZACION_PRODUCTO_COTIZACION_VENDEDOR_RUC')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
