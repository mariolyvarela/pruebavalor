<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CotizacionFinalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cotizacion Finals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cotizacion-final-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cotizacion Final', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'total',
            'iva',
            'fecha',
            'PAQUETE_id',
            //'COTIZACION_PRODUCTO_id',
            //'COTIZACION_PRODUCTO_PRODUCTO_id',
            //'COTIZACION_PRODUCTO_COTIZACION_id',
            //'COTIZACION_PRODUCTO_COTIZACION_CLIENTE_RUC',
            //'COTIZACION_PRODUCTO_COTIZACION_VENDEDOR_RUC',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
