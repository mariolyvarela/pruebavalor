<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CotizacionFinal */

$this->title = 'Update Cotizacion Final: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Cotizacion Finals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cotizacion-final-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
