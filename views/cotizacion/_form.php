<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cliente;
use app\models\Vendedor;
use app\models\Producto;
use app\models\Paquete;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Cotizacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cotizacion-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <?= $form->field($model, 'CLIENTE_RUC')->dropDownList(
                        ArrayHelper::map(Cliente::find()->all(),'RUC','nombre'),
                        ['prompt'=>'Seleccionar Cliente'])  ?>

    <?= $form->field($model, 'VENDEDOR_RUC')->dropDownList(
                        ArrayHelper::map(Vendedor::find()->all(),'RUC','nombre'),
                        ['prompt'=>'Seleccionar Vendedor']) ?>

    <?= $form->field($model, 'iva')->textInput() ?>

    <div class="row">

<div class="panel panel-default">
    <div class="panel-heading">
        <h4><i class="glyphicon glyphicon-envelope"></i> Productos</h4>
    </div>
    <div class="panel-body">
        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 10, // the maximum times, an element can be cloned (default 999)
            'min' => 1, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $CotizacionProductos[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'PRODUCTO_id',
                'cantidad',               
            ],
        ]); ?>

        <div class="container-items"><!— widgetContainer —>
        <?php foreach ($CotizacionProductos as $i => $CotizacionProducto): ?>
            <div class="item panel panel-default"><!— widgetBody —>
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Producto</h3>
                    <div class="pull-right">
                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <?= $form->field($CotizacionProducto, "[{$i}]PRODUCTO_id")->dropDownList(
                        ArrayHelper::map(Producto::find()->all(),'id','descripcion'),
                        ['prompt'=>'Seleccionar Producto']) 

                    ?>
                  
                    <div class="row">
                        <div class="col-sm-6">
                        <?= $form->field($CotizacionProducto, "[{$i}]cantidad")->textInput(['maxlength' => true]) ?>
                        </div>

                    </div><!— .row —>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
        <?php DynamicFormWidget::end(); ?>
    </div>
</div>
</div>

<div class="panel panel-default">
        <div class="panel-heading">
            <h4><i class="glyphicon glyphicon-envelope"></i> Paquetes</h4>
        </div>
        <div class="panel-body">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_package', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items-package', // required: css class selector
                'widgetItem' => '.item-package', // required: css class
                'limit' => 10, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item-package', // css class
                'deleteButton' => '.remove-item-package', // css class
                'model' => $CotizacionPaquetes[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'PAQUETE_id',
                    'iva',
                    'fecha'
                ],
            ]); ?>

            <div class="container-items-package"><!— widgetContainer —>
            <?php foreach ($CotizacionPaquetes as $i => $CotizacionPaquete): ?>
                <div class="item-package panel panel-default"><!— widgetBody —>
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Paquete</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item-package btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item-package btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?= $form->field($CotizacionPaquete, "[{$i}]PAQUETE_id")->dropDownList(
                            ArrayHelper::map(Paquete::find()->all(),'id','nombre'),
                            ['prompt'=>'Seleccionar Paquete']) 

                        ?>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
