<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Vendedor;
use app\models\Cliente;
use app\models\Producto;
use app\models\PaqueteProducto;

/* @var $this yii\web\View */
/* @var $model app\models\Cotizacion */


$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cotizaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$query=(new \yii\db\Query())->select('descuento')->from('Paquete_Producto')->where('id=1');
$command = $query->createCommand();
?>

<div class="cotizacion-view">
        <div class="container">
            <div class="row">
                <div class="col-md-8 factura titulo">
                    <h3>COTIZACIÓN</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 factura">
                    <h4>Vendedor:<?php
                        $vendedor = Vendedor::findOne($model->VENDEDOR_RUC);
                        echo " ".$vendedor->nombre;
                        
                    ?></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 factura">
                    <h4>Nombre Cliente:<?php
                        $cliente = Cliente::findOne($model->CLIENTE_RUC);
                        echo " ".$cliente->nombre;
                    ?></h4>
                </div>
                <div class="col-md-4 factura">
                    <h4>RUC:<?php
                         echo " ".$cliente->RUC;  
                    ?></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 factura">
                    <h4>Producto:</h4>
                </div>
                <div class="col-md-3 factura">
                    <h4>Cantidad:</h4>
                </div>
                <div class="col-md-2 factura">
                    <h4>Precio:</h4>
                </div>
            </div>
            <?php
          
            $acum=0;

            foreach($model->cotizacionProductos as $productos){
               
            ?>
            

            <div class="row">
                <div class="col-md-3 factura">
                    <h4>
                        <?php
                            echo " ".$productos->pRODUCTO->descripcion;
                            

                        ?>
                    </h4>
                </div>
                <div class="col-md-3 factura">
                    <h4>
                        <?php
                         echo " ".$productos->cantidad;
                         ?>
                    </h4>
                </div>
                <div class="col-md-2 factura">
                    <h4>
                        <?php
                         echo " ".$productos->pRODUCTO->precio;
                         ?>
                    </h4>
                </div>
            </div>
            
            <?php
            $acum=($productos->pRODUCTO->precio)*($productos->cantidad)+$acum;  
    }
                    /*$descuento2 = PaqueteProducto::findOne($productos->PRODUCTO_id);
                    echo " ".$descuento2->descuento[0]."%";  */
            ?> 

            <?php 
                $acum2 = 0;
                foreach($model->cotizacionFinals as $paquetes){
            ?>
                <div class="row">
                <div class="col-md-8 factura">
                    <h4>Paquetes:<?php
                            echo " ".$paquetes->pAQUETE->nombre;
                    ?></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 factura">
                    <h4>Precio: <?php
                        $preciopaq = PaqueteProducto::findOne($paquetes->PAQUETE_id);
                        echo " ".$preciopaq->subtotal;
                        $acum2 = $preciopaq->subtotal + $acum2; 
                        $desc2 = PaqueteProducto::findOne($paquetes->PAQUETE_id);
                    ?></h4>
                </div>
            </div>
            <?php }?>
            <div class="row">
                <div class="col-md-8 factura">
                    <h4>IVA:<?php
                        echo " ".$model->iva."%";
                    ?></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 factura">
                    <h4>Descuento:<?php
                            /*$descuento2 = PaqueteProducto::findOne($productos->PRODUCTO_id);*/
                            echo " ".$desc2->descuento."%"; 
                    ?></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 factura">
                    <h4>Precio:<?php
                        echo " ".$acum+$acum2 + ($acum+$acum2*$model->iva)/100 - ($acum+$acum2*$desc2->descuento)/100;
                    ?></h4>
                </div>
            </div>
        </div>

</div>
