<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CotizacionProductoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cotizacion Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cotizacion-producto-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cotizacion Producto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'PRODUCTO_id',
            'cantidad',
            'COTIZACION_id',
            'COTIZACION_CLIENTE_RUC',
            //'COTIZACION_VENDEDOR_RUC',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
