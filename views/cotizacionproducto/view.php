<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CotizacionProducto */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cotizacion Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cotizacion-producto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'PRODUCTO_id' => $model->PRODUCTO_id, 'COTIZACION_id' => $model->COTIZACION_id, 'COTIZACION_CLIENTE_RUC' => $model->COTIZACION_CLIENTE_RUC, 'COTIZACION_VENDEDOR_RUC' => $model->COTIZACION_VENDEDOR_RUC], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'PRODUCTO_id' => $model->PRODUCTO_id, 'COTIZACION_id' => $model->COTIZACION_id, 'COTIZACION_CLIENTE_RUC' => $model->COTIZACION_CLIENTE_RUC, 'COTIZACION_VENDEDOR_RUC' => $model->COTIZACION_VENDEDOR_RUC], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'PRODUCTO_id',
            'cantidad',
            'COTIZACION_id',
            'COTIZACION_CLIENTE_RUC',
            'COTIZACION_VENDEDOR_RUC',
        ],
    ]) ?>

</div>
