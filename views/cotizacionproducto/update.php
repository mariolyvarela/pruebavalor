<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CotizacionProducto */

$this->title = 'Update Cotizacion Producto: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cotizacion Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'PRODUCTO_id' => $model->PRODUCTO_id, 'COTIZACION_id' => $model->COTIZACION_id, 'COTIZACION_CLIENTE_RUC' => $model->COTIZACION_CLIENTE_RUC, 'COTIZACION_VENDEDOR_RUC' => $model->COTIZACION_VENDEDOR_RUC]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cotizacion-producto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
