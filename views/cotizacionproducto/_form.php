<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CotizacionProducto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cotizacion-producto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PRODUCTO_id')->textInput() ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <?= $form->field($model, 'COTIZACION_id')->textInput() ?>

    <?= $form->field($model, 'COTIZACION_CLIENTE_RUC')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COTIZACION_VENDEDOR_RUC')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
