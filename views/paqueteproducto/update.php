<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaqueteProducto */

$this->title = 'Update Paquete Producto: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Paquete Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'PRODUCTO_id' => $model->PRODUCTO_id, 'PAQUETE_id' => $model->PAQUETE_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paquete-producto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
