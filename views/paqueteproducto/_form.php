<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaqueteProducto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paquete-producto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PRODUCTO_id')->textInput() ?>

    <?= $form->field($model, 'PAQUETE_id')->textInput() ?>

    <?= $form->field($model, 'descuento')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subtotal')->textInput() ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
