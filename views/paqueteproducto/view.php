<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PaqueteProducto */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Paquete Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paquete-producto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'PRODUCTO_id' => $model->PRODUCTO_id, 'PAQUETE_id' => $model->PAQUETE_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'PRODUCTO_id' => $model->PRODUCTO_id, 'PAQUETE_id' => $model->PAQUETE_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'PRODUCTO_id',
            'PAQUETE_id',
            'descuento',
            'descripcion',
            'subtotal',
            'cantidad',
        ],
    ]) ?>

</div>
