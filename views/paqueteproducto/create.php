<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PaqueteProducto */

$this->title = 'Create Paquete Producto';
$this->params['breadcrumbs'][] = ['label' => 'Paquete Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paquete-producto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
