<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaqueteProductoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Paquete Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paquete-producto-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Paquete Producto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'PRODUCTO_id',
            'PAQUETE_id',
            'descuento',
            'descripcion',
            //'subtotal',
            //'cantidad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
